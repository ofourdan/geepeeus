/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Olivier Fourdan <ofourdan@redhat.com>
 */

#ifndef GEEPEEUS_GPU_H
#define GEEPEEUS_GPU_H

#include <glib-object.h>
#include <gudev/gudev.h>

G_BEGIN_DECLS

#define GEEPEEUS_TYPE_GPU (geepeeus_gpu_get_type ())
G_DECLARE_FINAL_TYPE (GeepeeusGpu, geepeeus_gpu, GEEPEEUS, GPU, GObject);

GeepeeusGpu * geepeeus_gpu_new                  (GUdevDevice *gudev);
const char  * geepeeus_gpu_get_gl_renderer      (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_gl_vendor        (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_gl_driver        (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_kernel_driver    (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_vendor_id        (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_device_id        (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_vendor_name      (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_device_name      (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_device_subsystem (GeepeeusGpu *gpu);
const char  * geepeeus_gpu_get_device_path      (GeepeeusGpu *gpu);
gboolean      geepeeus_gpu_is_platform_device   (GeepeeusGpu *gpu);
gboolean      geepeeus_gpu_is_boot_vga          (GeepeeusGpu *gpu);

G_END_DECLS

#endif /* GEEPEEUS_GPU_H */
