/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Olivier Fourdan <ofourdan@redhat.com>
 */

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <geepeeus/geepeeus-gpu.h>

#include "geepeeus-egl-helpers.h"
#include "geepeeus-udev-helpers.h"

struct _GeepeeusGpu
{
  GObject parent;

  GUdevDevice *gudev;
  EGLDisplay display;
  EGLDisplay context;
  int fd;

  char *renderer;
  char *vendor;
  char *driver;

  guint delayed_cleanup_id;
};

#define GEEPEEUS_TYPE_GPU (geepeeus_gpu_get_type ())
G_DEFINE_TYPE (GeepeeusGpu, geepeeus_gpu, G_TYPE_OBJECT)

static gboolean
geepeeus_gpu_delayed_cleanup (gpointer data)
{
  GeepeeusGpu *gpu = GEEPEEUS_GPU (data);

  g_debug("%s", __func__);

  gpu->delayed_cleanup_id = 0;

  if (gpu->context != EGL_NO_CONTEXT)
    {
      geepeeus_egl_destroy_context (gpu->display, gpu->context);
      gpu->context = EGL_NO_CONTEXT;
    }

  if (gpu->display != EGL_NO_DISPLAY)
    {
      geepeeus_egl_terminate_display (gpu->display);
      gpu->display = EGL_NO_DISPLAY;
    }

  if (gpu->fd >= 0)
    {
      close (gpu->fd);
      gpu->fd = -1;
    }

  return G_SOURCE_REMOVE;
}


GeepeeusGpu *
geepeeus_gpu_new (GUdevDevice *gudev)
{
  GeepeeusGpu *gpu;
  const char *device_path;
  EGLDisplay display;
  EGLDisplay context;
  int fd;

  device_path = g_udev_device_get_device_file (gudev);
  g_debug("%s: device path='%s'", __func__, device_path);

  gpu = g_object_new (GEEPEEUS_TYPE_GPU, NULL);

  gpu->gudev = gudev;
  g_object_ref (gpu->gudev);

  fd = open (device_path, O_RDWR, 0);
  if (fd < 0)
    {
      g_debug("%s: Failed to open device, %s", __func__, strerror (errno));
      return gpu;
    }

  /* First try with EGLStream */
  display = geepeeus_egl_get_device_display (device_path, fd);

  /* If no match, try with GBM, that should always work */
  if (display == EGL_NO_DISPLAY)
    display = geepeeus_egl_get_gbm_display (fd);

  if (display == EGL_NO_DISPLAY)
    {
      g_debug("%s: Failed to get EGL display", __func__);
      goto fail;
    }

  if (!geepeeus_egl_init_display (display))
    {
      g_debug("%s: Failed to open EGL display", __func__);
      goto fail;
    }

  context = geepeeus_egl_setup_context (display);
  if (context == EGL_NO_CONTEXT)
    {
      g_debug("%s: Failed to setup EGL context", __func__);
      geepeeus_egl_terminate_display (display);
      goto fail;
    }

  gpu->renderer = geepeeus_egl_get_gl_renderer (display);
  gpu->vendor = geepeeus_egl_get_gl_vendor (display);
  gpu->driver = geepeeus_egl_get_gl_driver (display);

  gpu->display = display;
  gpu->context = context;

  gpu->delayed_cleanup_id = g_idle_add (geepeeus_gpu_delayed_cleanup, gpu);

  return gpu;

fail:
  close (fd);

  return gpu;
}

const char *
geepeeus_gpu_get_gl_renderer (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return gpu->renderer;
}

const char *
geepeeus_gpu_get_gl_vendor (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return gpu->vendor;
}

const char *
geepeeus_gpu_get_gl_driver (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return gpu->driver;
}

const char *
geepeeus_gpu_get_kernel_driver (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return geepeeus_udev_get_driver (gpu->gudev);
}

const char *
geepeeus_gpu_get_vendor_id (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return geepeeus_udev_get_vendor_id (gpu->gudev);
}

const char *
geepeeus_gpu_get_device_id (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return geepeeus_udev_get_device_id (gpu->gudev);
}

const char *
geepeeus_gpu_get_vendor_name (GeepeeusGpu *gpu)
{
  GUdevDevice *gudev_parent;

  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);

  gudev_parent = g_udev_device_get_parent (gpu->gudev);
  return g_udev_device_get_property (gudev_parent, "ID_VENDOR_FROM_DATABASE");
}

const char *
geepeeus_gpu_get_device_name (GeepeeusGpu *gpu)
{
  GUdevDevice *gudev_parent;

  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);

  gudev_parent = g_udev_device_get_parent (gpu->gudev);
  return g_udev_device_get_property (gudev_parent, "ID_MODEL_FROM_DATABASE");
}

const char *
geepeeus_gpu_get_device_subsystem (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return geepeeus_udev_get_device_subsystem (gpu->gudev);
}

const char *
geepeeus_gpu_get_device_path (GeepeeusGpu *gpu)
{
  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), NULL);
  return g_udev_device_get_device_file (gpu->gudev);
}

gboolean
geepeeus_gpu_is_platform_device (GeepeeusGpu *gpu)
{
  g_autoptr (GUdevDevice) platform_device = NULL;

  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), FALSE);
  platform_device =
      g_udev_device_get_parent_with_subsystem (gpu->gudev, "platform", NULL);

  return !!platform_device;
}


gboolean
geepeeus_gpu_is_boot_vga (GeepeeusGpu *gpu)
{
  g_autoptr (GUdevDevice) pci_device = NULL;

  g_return_val_if_fail (GEEPEEUS_IS_GPU (gpu), FALSE);
  pci_device =
     g_udev_device_get_parent_with_subsystem (gpu->gudev, "pci", NULL);

  if (!pci_device)
    return FALSE;

  return g_udev_device_get_sysfs_attr_as_int (pci_device, "boot_vga") == 1;
}

static void
geepeeus_gpu_finalize (GObject *object)
{
  GeepeeusGpu *gpu = GEEPEEUS_GPU (object);

  g_debug("%s", __func__);

  if (gpu->delayed_cleanup_id)
    geepeeus_gpu_delayed_cleanup (gpu);

  g_object_unref (gpu->gudev);

  g_free (gpu->renderer);
  g_free (gpu->vendor);
  g_free (gpu->driver);

  G_OBJECT_CLASS (geepeeus_gpu_parent_class)->finalize (object);
}

static void
geepeeus_gpu_class_init (GeepeeusGpuClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = geepeeus_gpu_finalize;
}

static void
geepeeus_gpu_init (GeepeeusGpu *gpu)
{
}
