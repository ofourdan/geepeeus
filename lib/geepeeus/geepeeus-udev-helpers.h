/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef GEEPEEUS_UDEV_HELPERS_H
#define GEEPEEUS_UDEV_HELPERS_H

#include <gudev/gudev.h>

G_BEGIN_DECLS

const char * geepeeus_udev_get_driver           (GUdevDevice *gudev);
const char * geepeeus_udev_get_vendor_id        (GUdevDevice *gudev);
const char * geepeeus_udev_get_device_id        (GUdevDevice *gudev);
const char * geepeeus_udev_get_device_subsystem (GUdevDevice *gudev);
GList *      geepeeus_list_drm_devices          (GUdevClient *gudev_client);

G_END_DECLS

#endif /* GEEPEEUS_UDEV_HELPERS_H */

