/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#include "geepeeus-udev-helpers.h"

static gboolean
is_drm_device (GUdevDevice *gudev)
{
  const char *device_type;

  if (g_udev_device_get_device_type (gudev) != G_UDEV_DEVICE_TYPE_CHAR)
    return FALSE;

  device_type = g_udev_device_get_property (gudev, "DEVTYPE");
  if (g_strcmp0 (device_type, "drm_minor") != 0)
    return FALSE;

  return TRUE;
}

static const char *
search_sysfs_attr (GUdevDevice *gudev,
                   const char  *attr)
{
  GUdevDevice *d;

  for (d = gudev; d; d = g_udev_device_get_parent (d))
    {
      const char *value = g_udev_device_get_sysfs_attr (d, attr);
      if (value)
        return value;
    }

  return NULL;
}

const char *
geepeeus_udev_get_driver (GUdevDevice *gudev)
{
  GUdevDevice *d;

  for (d = gudev; d; d = g_udev_device_get_parent (d))
    {
      const char *driver = g_udev_device_get_driver (d);
      if (driver && strlen (driver))
        return driver;
    }

  return NULL;
}

const char *
geepeeus_udev_get_vendor_id (GUdevDevice *gudev)
{
  return search_sysfs_attr (gudev, "vendor");
}

const char *
geepeeus_udev_get_device_id (GUdevDevice *gudev)
{
  return search_sysfs_attr (gudev, "device");
}

const char *
geepeeus_udev_get_device_subsystem (GUdevDevice *gudev)
{
  GUdevDevice *d = gudev;

  for (d = gudev; d; d = g_udev_device_get_parent (d))
    {
      const char *subsystem = g_udev_device_get_subsystem (d);
      if (subsystem && g_strcmp0 (subsystem, "drm") != 0)
        return subsystem;
    }

  return NULL;
}

GList *
geepeeus_list_drm_devices (GUdevClient *gudev_client)
{
  g_autoptr (GUdevEnumerator) enumerator = NULL;
  GList *devices;
  GList *l;

  enumerator = g_udev_enumerator_new (gudev_client);

  g_udev_enumerator_add_match_name (enumerator, "card*");
  g_udev_enumerator_add_match_subsystem (enumerator, "drm");

  devices = g_udev_enumerator_execute (enumerator);
  if (!devices)
    return NULL;

  for (l = devices; l;)
    {
      GUdevDevice *device = l->data;
      GList *l_next = l->next;

      if (!is_drm_device (device))
        {
          g_object_unref (device);
          devices = g_list_delete_link (devices, l);
        }

      l = l_next;
    }

  return devices;
}
