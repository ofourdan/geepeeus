/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Olivier Fourdan <ofourdan@redhat.com>
 */

#ifndef EGL_HELPERS_H
#define EGL_HELPERS_H

#include <EGL/egl.h>

G_BEGIN_DECLS

EGLDisplay geepeeus_egl_get_device_display (const char *kms_file_path,
                                            int         kms_fd);
EGLDisplay geepeeus_egl_get_gbm_display    (int         kms_fd);
gboolean   geepeeus_egl_init_display       (EGLDisplay  display);
void       geepeeus_egl_terminate_display  (EGLDisplay  display);
EGLContext geepeeus_egl_setup_context      (EGLDisplay  display);
void       geepeeus_egl_destroy_context    (EGLDisplay  display,
                                            EGLContext  context);
char *     geepeeus_egl_get_gl_renderer    (EGLDisplay  display);
char *     geepeeus_egl_get_gl_vendor      (EGLDisplay  display);
char *     geepeeus_egl_get_gl_driver      (EGLDisplay  display);

G_END_DECLS

#endif /* EGL_HELPERS_H */
