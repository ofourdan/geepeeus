/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Olivier Fourdan <ofourdan@redhat.com>
 */

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GL/gl.h>
#include <gbm.h>
#include <glib.h>

static const char *
geepeeus_egl_get_extensions (EGLDisplay display)
{
  const char *extensions;

  extensions = eglQueryString(display, EGL_EXTENSIONS);
  g_debug("%s: extensions='%s'", __func__, extensions);

  return extensions;
}

static gboolean
geepeeus_egl_has_extension (const char *extensions,
                            const char *extension)
{
  char **extensionsv;
  gboolean found;

  if (!extensions)
    return FALSE;

  extensionsv = g_strsplit (extensions, " ", -1);
  found = g_strv_contains ((const gchar * const *) extensionsv, extension);
  g_strfreev (extensionsv);

  g_debug ("%s: '%s' %sfound", __func__, extension, found ? "" : "not ");

  return found;
}

static EGLDisplay
geepeeus_egl_get_platform_display (EGLenum       platform,
                                   void         *native_display,
                                   const EGLint *attribs)
{
  PFNEGLGETPLATFORMDISPLAYEXTPROC getPlatformDisplay;

  getPlatformDisplay = (PFNEGLGETPLATFORMDISPLAYEXTPROC)
                           eglGetProcAddress ("eglGetPlatformDisplayEXT");

  return getPlatformDisplay (platform, native_display, attribs);
}

EGLDisplay
geepeeus_egl_get_device_display (const char *kms_file_path,
                                 int         kms_fd)
{
  const char *extensions;
  EGLDeviceEXT egl_device;
  EGLDeviceEXT *devices;
  int num_devices, i;
  PFNEGLQUERYDEVICESEXTPROC queryDevices;
  PFNEGLQUERYDEVICESTRINGEXTPROC queryDeviceString;
  EGLint attribs[] = {
    EGL_DRM_MASTER_FD_EXT, kms_fd,
    EGL_NONE
  };

  extensions = geepeeus_egl_get_extensions (EGL_NO_DISPLAY);
  if (!extensions)
    return EGL_NO_DISPLAY;

  if (!geepeeus_egl_has_extension (extensions, "EGL_EXT_platform_base"))
    {
      g_debug ("%s: Missing extension 'EGL_EXT_platform_base'", __func__);
      return EGL_NO_DISPLAY;
    }

  if (!geepeeus_egl_has_extension (extensions, "EGL_EXT_device_base"))
    {
      g_debug ("%s: Missing extension 'EGL_EXT_device_base'", __func__);
      return EGL_NO_DISPLAY;
    }

  if (!geepeeus_egl_has_extension (extensions, "EGL_EXT_device_drm"))
    {
      g_debug ("%s: Missing extension 'EGL_EXT_device_drm'", __func__);
      return EGL_NO_DISPLAY;
    }

  queryDevices = (PFNEGLQUERYDEVICESEXTPROC)
                     eglGetProcAddress ("eglQueryDevicesEXT");

  if (!queryDevices (0, NULL, &num_devices))
    {
      g_debug ("%s: Failed to query devices", __func__);
      return EGL_NO_DISPLAY;
    }
  devices = g_new0 (EGLDeviceEXT, num_devices);
  if (!queryDevices (num_devices, devices, &num_devices))
    {
      g_free (devices);
      return EGL_NO_DISPLAY;
    }

  g_debug("%s: %i devices found", __func__, num_devices);
  queryDeviceString = (PFNEGLQUERYDEVICESTRINGEXTPROC)
                          eglGetProcAddress ("eglQueryDeviceStringEXT");

  egl_device = EGL_NO_DEVICE_EXT;
  for (i = 0; i < num_devices; i++)
    {
      const char *egl_device_drm_path;

      egl_device_drm_path = queryDeviceString (devices[i], EGL_DRM_DEVICE_FILE_EXT);
      if (!egl_device_drm_path)
        continue;

      if (strcmp (egl_device_drm_path, kms_file_path) == 0)
        {
          egl_device = devices[i];
          break;
        }
    }
  g_free (devices);

  return geepeeus_egl_get_platform_display (EGL_PLATFORM_DEVICE_EXT,
                                            egl_device, attribs);
}

EGLDisplay
geepeeus_egl_get_gbm_display (int kms_fd)
{
  const char *extensions;
  struct gbm_device *gbm_device;

  extensions = geepeeus_egl_get_extensions (EGL_NO_DISPLAY);
  if (!extensions)
    return EGL_NO_DISPLAY;

  if (!geepeeus_egl_has_extension (extensions, "EGL_MESA_platform_gbm") &&
      !geepeeus_egl_has_extension (extensions, "EGL_KHR_platform_gbm"))
    {
      g_debug ("%s: Missing extensions for GBM", __func__);
      return EGL_NO_DISPLAY;
    }

  gbm_device = gbm_create_device (kms_fd);
  if (!gbm_device)
    {
      g_debug ("%s: Failed to create GBM device'", __func__);
      return EGL_NO_DISPLAY;
    }

  return geepeeus_egl_get_platform_display (EGL_PLATFORM_GBM_KHR,
                                            gbm_device, NULL);
}

gboolean
geepeeus_egl_init_display (EGLDisplay display)
{
  if (eglInitialize (display, NULL, NULL) == EGL_FALSE)
    {
      g_debug ("%s: Failed to initialize display", __func__);
      return FALSE;
    }
  g_debug ("%s: display=%p", __func__, display);
  return TRUE;
}

void
geepeeus_egl_terminate_display (EGLDisplay display)
{
  g_debug ("%s: display=%p", __func__, display);
  if (!eglTerminate (display))
    g_debug ("%s: Failed to terminate display=%p", __func__, display);
}

EGLContext
geepeeus_egl_setup_context (EGLDisplay display)
{
  EGLContext context;

  context = eglCreateContext (display, NULL, EGL_NO_CONTEXT, NULL);
  if (context == EGL_NO_CONTEXT)
    {
      g_debug ("%s: Failed to create context", __func__);
      return EGL_NO_CONTEXT;
    }

  if (!eglMakeCurrent (display, EGL_NO_SURFACE, EGL_NO_SURFACE, context))
    {
      g_debug ("%s: Failed to make context current", __func__);
      eglDestroyContext (display, context);
      return EGL_NO_CONTEXT;
    }

  g_debug ("%s: context=%p", __func__, context);
  return context;
}

void
geepeeus_egl_destroy_context (EGLDisplay display,
                              EGLContext context)
{
  g_debug ("%s: context=%p for display=%p", __func__, context, display);
  if (!eglDestroyContext (display, context))
    g_debug ("%s: Failed to destroy context=%p", __func__, context);
}

char *
geepeeus_egl_get_gl_renderer (EGLDisplay display)
{
  char* renderer = NULL;

  renderer = g_strdup ((const char *) glGetString (GL_RENDERER));
  if (!renderer)
    {
      g_debug ("%s: Failed to get GL renderer", __func__);
      return NULL;
    }

  g_debug ("%s: GL renderer='%s'\n", __func__, renderer);

  return renderer;
}

char *
geepeeus_egl_get_gl_vendor (EGLDisplay display)
{
  char* vendor = NULL;

  vendor = g_strdup ((const char *) glGetString (GL_VENDOR));
  if (!vendor)
    {
      g_debug ("%s: Failed to get GL vendor", __func__);
      return NULL;
    }

  g_debug ("%s: GL vendor='%s'\n", __func__, vendor);

  return vendor;
}

char *
geepeeus_egl_get_gl_driver (EGLDisplay display)
{
  char* driver = NULL;
#ifdef EGL_MESA_query_driver
  const char *extensions;

  extensions = geepeeus_egl_get_extensions (display);
  if (!extensions)
    return NULL;

  if (geepeeus_egl_has_extension (extensions, "EGL_MESA_query_driver"))
    {
      PFNEGLGETDISPLAYDRIVERNAMEPROC getDisplayDriverName;

      getDisplayDriverName = (PFNEGLGETDISPLAYDRIVERNAMEPROC)
                                 eglGetProcAddress ("eglGetDisplayDriverName");
      driver = g_strdup ((const char *) getDisplayDriverName(display));
      g_debug ("%s: GL driver='%s'\n", __func__, driver);
    }
#endif

  return driver;
}
