/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Olivier Fourdan <ofourdan@redhat.com>
 */

#ifndef GEEPEEUS_SYS_H
#define GEEPEEUS_SYS_H

#include <glib.h>
#include <glib-object.h>

#include <geepeeus/geepeeus-gpu.h>

G_BEGIN_DECLS

#define GEEPEEUS_TYPE_SYS (geepeeus_sys_get_type ())
G_DECLARE_FINAL_TYPE (GeepeeusSys, geepeeus_sys, GEEPEEUS, SYS, GObject);

GeepeeusSys * geepeeus_sys_new          (void);
GList       * geepeeus_sys_get_gpu_list (GeepeeusSys *geepeeus_sys);

G_END_DECLS

#endif /* GEEPEEUS_SYS_H */
