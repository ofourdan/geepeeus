/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Olivier Fourdan <ofourdan@redhat.com>
 */

#include <glib.h>
#include <geepeeus/geepeeus-sys.h>

#include "geepeeus-egl-helpers.h"
#include "geepeeus-udev-helpers.h"

/**
 * GeepeeusSys:
 *
 * #GeepeeusSys represents the set of graphics devices on the system.
 */
struct _GeepeeusSys
{
  GObject parent;

  GUdevClient *gudev_client;
  GList *geepeeus_devices;
};

enum
{
  GPU_ADDED,
  GPU_REMOVED,
  N_SIGNALS,
};

static guint signals[N_SIGNALS];

#define GEEPEEUS_TYPE_SYS (geepeeus_sys_get_type ())
G_DEFINE_TYPE (GeepeeusSys, geepeeus_sys, G_TYPE_OBJECT)

static int
find_gpu_by_device_path (gconstpointer a,
                         gconstpointer b)
{
  const char *device_path_a;
  const char *device_path_b = b;

  device_path_a = geepeeus_gpu_get_device_path ((GeepeeusGpu *) a);
  g_debug ("%s: a='%s' b=%s", __func__, device_path_a, device_path_b);

  return strcmp (device_path_a, device_path_b);
}

static int
sort_gpu_by_device_path (gconstpointer a,
                         gconstpointer b)
{
  const char *device_path_a;
  const char *device_path_b;

  device_path_a = geepeeus_gpu_get_device_path ((GeepeeusGpu *) a);
  device_path_b = geepeeus_gpu_get_device_path ((GeepeeusGpu *) b);
  g_debug ("%s: a='%s' b=%s", __func__, device_path_a, device_path_b);

  return strcmp (device_path_a, device_path_b);
}

static void
geepeeus_sys_on_uevent (GUdevClient *client,
                        const char  *action,
                        GUdevDevice *device,
                        gpointer     user_data)
{
  GeepeeusSys *geepeeus_sys = GEEPEEUS_SYS (user_data);
  const char *device_path = g_udev_device_get_device_file (device);
  GList *l;

  if (!device_path)
    return;

  l = g_list_find_custom (geepeeus_sys->geepeeus_devices,
                          device_path,
                          find_gpu_by_device_path);

  if (g_str_equal (action, "add"))
    {
      if (l == NULL)
        {
          geepeeus_sys->geepeeus_devices =
              g_list_insert_sorted (geepeeus_sys->geepeeus_devices,
                                    geepeeus_gpu_new (device),
                                    sort_gpu_by_device_path);
          g_signal_emit (geepeeus_sys, signals[GPU_ADDED], 0, device_path);
        }
    }
  else if (g_str_equal (action, "remove"))
    {
      if (l != NULL)
        {
          g_object_unref (G_OBJECT (l->data));
          geepeeus_sys->geepeeus_devices =
              g_list_remove (geepeeus_sys->geepeeus_devices, l);
          g_signal_emit (geepeeus_sys, signals[GPU_REMOVED], 0, device_path);
        }
    }
}


static void
geepeeus_sys_constructed (GObject *object)
{
  const char *subsystems[] = { "drm", NULL };
  GeepeeusSys *geepeeus_sys = GEEPEEUS_SYS (object);
  GList *devices, *l;

  geepeeus_sys->gudev_client = g_udev_client_new (subsystems);

  devices = geepeeus_list_drm_devices (geepeeus_sys->gudev_client);
  for (l = devices; l; l = g_list_next (l))
    {
      GUdevDevice *device = l->data;
      geepeeus_sys->geepeeus_devices =
          g_list_insert_sorted (geepeeus_sys->geepeeus_devices,
                                geepeeus_gpu_new (device),
                                sort_gpu_by_device_path);
      g_signal_emit (geepeeus_sys, signals[GPU_ADDED], 0);

    }
  g_list_free (devices);

  g_signal_connect (geepeeus_sys->gudev_client,
                    "uevent",
                    G_CALLBACK (geepeeus_sys_on_uevent),
                    geepeeus_sys);
}

static void
geepeeus_sys_finalize (GObject *object)
{
  GeepeeusSys *geepeeus_sys = GEEPEEUS_SYS (object);

  g_list_free_full (geepeeus_sys->geepeeus_devices, g_object_unref);
  g_clear_object (&geepeeus_sys->gudev_client);

  G_OBJECT_CLASS (geepeeus_sys_parent_class)->finalize (object);
}

static void
geepeeus_sys_init (GeepeeusSys *geepeeus_sys)
{
}

static void
geepeeus_sys_class_init (GeepeeusSysClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = geepeeus_sys_constructed;
  object_class->finalize    = geepeeus_sys_finalize;

  /**
   * GeepeeusSys::gpu-added:
   * @device_path: Path to the device being added.
   *
   * Emitted when a new #GeepeeusGpu is added to the @GeepeeusSys.
   *
   */
  signals[GPU_ADDED] = g_signal_new ("gpu-added",
                                     G_TYPE_FROM_CLASS (klass),
                                     G_SIGNAL_RUN_LAST,
                                     0,
                                     NULL, NULL, NULL,
                                     G_TYPE_NONE,
                                     1, G_TYPE_STRING);

  /**
   * GeepeeusSys::gpu-removed:
   * @device_path: Path to the device being removed.
   *
   * Emitted when a new #GeepeeusGpu is removed from the @GeepeeusSys.
   *
   */
  signals[GPU_REMOVED] = g_signal_new ("gpu-removed",
                                       G_TYPE_FROM_CLASS (klass),
                                       G_SIGNAL_RUN_LAST,
                                       0,
                                       NULL, NULL, NULL,
                                       G_TYPE_NONE,
                                       1, G_TYPE_STRING);
}

/**
 * geepeeus_sys_new:
 *
 * Creates a new #GeepeeusSys and query the #GeepeeusGpu devices on the
 * system.
 *
 * Return value: (transfer full): a new #GeepeeusSys
 */
GeepeeusSys *
geepeeus_sys_new (void)
{
  return GEEPEEUS_SYS (g_object_new (GEEPEEUS_TYPE_SYS, NULL));
}

/**
 * geepeeus_sys_get_gpu_list:
 * @geepeeus_sys: The #GeepeeusSys
 *
 * Returns a GList of #GeepeeusGpu devices on the system
 *
 * Return value: (transfer full): a GList of #GeepeeusGpu
 */
GList *
geepeeus_sys_get_gpu_list (GeepeeusSys *geepeeus_sys)
{
  g_return_val_if_fail (GEEPEEUS_IS_SYS (geepeeus_sys), NULL);

  return g_list_copy (geepeeus_sys->geepeeus_devices);
}

