/*
 * Copyright (C) 2019 Red Hat
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Author: Olivier Fourdan <ofourdan@redhat.com>
 */

#include <gio/gio.h>
#include <glib.h>

#include <geepeeus/geepeeus-sys.h>

#include "geepeeus-dbus-gpu.h"

#define GEEPEEUS_GPU_DBUS_SERVICE "org.freedesktop.Geepeeus"
#define GEEPEEUS_GPU_DBUS_PATH "/org/freedesktop/Geepeeus/Gpu"

typedef struct _GeepeeusApp
{
  GApplication parent;

  GDBusConnection *connection;
  GeepeeusDBusGpu *proxy;

  GeepeeusSys *geepeeus_sys;
} GeepeeusApp;

#define GEEPEEUS_TYPE_APP (geepeeus_app_get_type ())
G_DECLARE_FINAL_TYPE (GeepeeusApp, geepeeus_app, GEEPEEUS, APP, GApplication);
G_DEFINE_TYPE (GeepeeusApp, geepeeus_app, G_TYPE_APPLICATION)

static void
on_gpu_added (GeepeeusSys *geepeeus_sys,
              const char  *device_path,
              gpointer     data)
{
  GeepeeusApp *geepeeus_app = GEEPEEUS_APP (data);

  g_debug ("%s: Device '%s' added", __func__, device_path);

  geepeeus_dbus_gpu_emit_device_added (geepeeus_app->proxy, device_path);
}

static void
on_gpu_removed (GeepeeusSys *geepeeus_sys,
                const char  *device_path,
                gpointer     data)
{
  GeepeeusApp *geepeeus_app = GEEPEEUS_APP (data);

  g_debug ("%s: Device '%s' removed", __func__, device_path);

  geepeeus_dbus_gpu_emit_device_removed (geepeeus_app->proxy, device_path);
}

static gboolean
on_get_devices (GeepeeusDBusGpu        *proxy,
                GDBusMethodInvocation  *invocation,
                GeepeeusApp            *geepeeus_app)
{
  GVariantBuilder res_builder;
  GList *l, *gpu_list;

  g_variant_builder_init (&res_builder,
                          G_VARIANT_TYPE ("a{sa{sv}}"));

  gpu_list = geepeeus_sys_get_gpu_list (geepeeus_app->geepeeus_sys);
  for (l = gpu_list; l; l = g_list_next (l))
    {
      GeepeeusGpu *gpu = (GeepeeusGpu *) (l->data);
      GVariantBuilder properties_builder;
      const char *str;

      g_variant_builder_init (&properties_builder,
                              G_VARIANT_TYPE ("a{sv}"));

      str = geepeeus_gpu_get_gl_renderer (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "gl-renderer",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_gl_vendor (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "gl-vendor",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_gl_driver (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "gl-driver",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_kernel_driver (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "kernel-driver",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_vendor_id (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "vendor-id",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_device_id (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "device-id",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_vendor_name (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "vendor-name",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_device_name (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "device-name",
                               g_variant_new_string (str));

      str = geepeeus_gpu_get_device_subsystem (gpu);
      if (str)
        g_variant_builder_add (&properties_builder, "{sv}",
                               "subsystem",
                               g_variant_new_string (str));

      g_variant_builder_add (&properties_builder, "{sv}",
                             "is-boot-vga",
                             g_variant_new_boolean (geepeeus_gpu_is_boot_vga (gpu)));

      g_variant_builder_add (&properties_builder, "{sv}",
                             "is-platform-device",
                             g_variant_new_boolean (geepeeus_gpu_is_platform_device (gpu)));

      g_variant_builder_add (&res_builder, "{sa{sv}}",
                             geepeeus_gpu_get_device_path (gpu),
                             &properties_builder);
    }
  g_list_free (gpu_list);

  geepeeus_dbus_gpu_complete_get_devices (proxy,
                                          invocation,
                                          g_variant_builder_end (&res_builder));

  return TRUE;
}

static void
geepeeus_app_init (GeepeeusApp *app)
{
  g_debug ("%s", __func__);
}

static void
geepeeus_app_startup (GApplication *app)
{
  GeepeeusApp *geepeeus_app = GEEPEEUS_APP (app);

  g_debug ("%s", __func__);

  geepeeus_app->geepeeus_sys = geepeeus_sys_new ();

  g_signal_connect (geepeeus_app->proxy,
                    "handle-get-devices",
                    G_CALLBACK (on_get_devices),
                    geepeeus_app);

  g_signal_connect (geepeeus_app->geepeeus_sys,
                    "gpu-added",
                    G_CALLBACK (on_gpu_added),
                    geepeeus_app);

  g_signal_connect (geepeeus_app->geepeeus_sys,
                    "gpu-removed",
                    G_CALLBACK (on_gpu_removed),
                    geepeeus_app);

  g_application_hold (app);

  G_APPLICATION_CLASS (geepeeus_app_parent_class)->startup (app);
}

static void
geepeeus_app_shutdown (GApplication *app)
{
  GeepeeusApp *geepeeus_app = GEEPEEUS_APP (app);

  g_debug ("%s", __func__);

  g_clear_object (&geepeeus_app->geepeeus_sys);
  G_APPLICATION_CLASS (geepeeus_app_parent_class)->shutdown (app);
}

static gboolean
geepeeus_app_dbus_register (GApplication     *app,
                            GDBusConnection  *connection,
                            const gchar      *object_path,
                            GError          **error)
{
  GeepeeusApp *geepeeus_app = GEEPEEUS_APP (app);

  g_debug ("%s", __func__);

  geepeeus_app->proxy = geepeeus_dbus_gpu_skeleton_new ();
  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (geepeeus_app->proxy),
                                    connection,
                                    GEEPEEUS_GPU_DBUS_PATH,
                                    NULL);

  return G_APPLICATION_CLASS (geepeeus_app_parent_class)->dbus_register (app,
                                                                         connection,
                                                                         object_path,
                                                                         error);
}

static void
geepeeus_app_dbus_unregister (GApplication    *app,
                              GDBusConnection *connection,
                              const gchar     *object_path)
{
  GeepeeusApp *geepeeus_app = GEEPEEUS_APP (app);

  g_debug ("%s", __func__);

  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (geepeeus_app->proxy));

  G_APPLICATION_CLASS (geepeeus_app_parent_class)->dbus_unregister (app,
                                                                    connection,
                                                                    object_path);
}

static void
geepeeus_app_class_init (GeepeeusAppClass *klass)
{
  GApplicationClass *g_application_class = G_APPLICATION_CLASS (klass);

  g_debug ("%s", __func__);
  g_application_class->startup = geepeeus_app_startup;
  g_application_class->shutdown = geepeeus_app_shutdown;
  g_application_class->dbus_register = geepeeus_app_dbus_register;
  g_application_class->dbus_unregister = geepeeus_app_dbus_unregister;
}

static void
on_terminate (GAction   *action,
              GVariant  *parameter,
              GApplication *app)
{
  g_debug ("%s", __func__);

  g_application_quit (app);
}

static void
send_terminate (GApplication *app)
{
  g_debug ("%s", __func__);

  g_application_register (app, NULL, NULL);
  g_action_group_activate_action (G_ACTION_GROUP (app), "terminate", NULL);
}

int
main (int argc, char *argv[])
{
  g_autoptr(GApplication) app = NULL;
  g_autoptr(GSimpleAction) action = NULL;
  gboolean terminate = FALSE;
  GOptionEntry entries[] = {
    { "terminate", 0, 0, G_OPTION_ARG_NONE, &terminate,
      "Terminates the daemon if running", NULL },
    { NULL }
  };
  GError *error = NULL;
  GOptionContext *context;

  g_debug ("%s", __func__);

  context = g_option_context_new (NULL);
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("Invalid option: %s\n", error->message);
      g_error_free (error);
      return -1;
    }

  if (terminate)
    {
      app = g_application_new (GEEPEEUS_GPU_DBUS_SERVICE, 0);
      send_terminate (app);

      return 0;
    }

  g_set_application_name ("Geepeeus");

  app = g_object_new (GEEPEEUS_TYPE_APP,
                      "application-id", GEEPEEUS_GPU_DBUS_SERVICE,
                      "flags", G_APPLICATION_IS_SERVICE,
                      "inactivity-timeout", 10 * 1000,
                      NULL);

  action = g_simple_action_new ("terminate", NULL);
  g_signal_connect (action, "activate", G_CALLBACK (on_terminate), app);
  g_action_map_add_action (G_ACTION_MAP (app), G_ACTION (action));

  return g_application_run (app, argc, argv);
}
